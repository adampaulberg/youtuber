const fs = require('fs');
const async = require('async');
const path = require("path");
const low = require('lowdb');
const ytdl = require('ytdl-core');
const FileAsync = require('lowdb/adapters/FileAsync');
const bluebird = require('bluebird')
const ytpl = bluebird.promisifyAll(require('ytpl'));
const ffmpeg = require('fluent-ffmpeg');
const fastify = require('fastify')({ logger: false });
const cds = require('check-disk-space');

const config = require("./config");
const mlog = require('./logging');
const notify = require('./notifications');

const max_downloaders = config.max_download;

let videos_to_download = [];
let videos_downloading = [];
let downloading_count = 0;
let db;
let download_state = {};
let firstload = true;
let pause = false;

fastify.register(require('point-of-view'), {
    engine: {
        handlebars: require('handlebars'),
    },
    includeViewExtension: true,
    defaultLayout: 'main', // Not doing anything
    templates: path.join(__dirname, 'views/'),
});

fastify.get('/youtuber/pause', async (req, reply) => {
    pause = !pause;
    return {new_status: pause};
});

fastify.get('*', async (req, reply) => {
    let downloading = [];

    for(let key of Object.keys(download_state)) {
        let v = download_state[key];
        v = JSON.parse(JSON.stringify(v));
        try {
            v.merge_percent = v.merge_percent.toFixed(2);
        }catch(e){}
        v.audio = v.audio !== 'missing';
        v.video = v.video !== 'missing';

        downloading.push(v);
    }

    let download_path = config.filepath;
    let download_dir = '';

    if(download_path.startsWith('.')){
        download_dir = path.join(__dirname, download_path);
    } else {
        download_dir = path.resolve(download_path);
    }

    let tmp_drive = await cds(path.resolve(__dirname, './tmp'));
    let download_drive = await cds(download_dir);

    tmp_drive.freeGB = (tmp_drive.free / 1024 / 1024 /1024).toFixed(2);
    tmp_drive.sizeGB = (tmp_drive.size / 1024 / 1024 /1024).toFixed(2);

    download_drive.freeGB = (download_drive.free / 1024 / 1024 /1024).toFixed(2);
    download_drive.sizeGB = (download_drive.size / 1024 / 1024 /1024).toFixed(2);

    let failures = await db.get("failures").cloneDeep().value();

    let data = {
        queue_count: videos_to_download.length,
        downloading: downloading,
        queue: videos_to_download,
        firstload: firstload,
        failures_count: failures.length,
        disk_space : {
            tmp: tmp_drive,
            storage: download_drive
        },
        is_paused: pause
    };

    reply.view('index', data);
});

/**
 * Pulls down a playlist for a specific youtube channel
 */
async function get_playlist(channel, consumed, failures) {
    mlog.debug('QUERYING:', channel.friendly);
    let count = 0;
    try {
        let results = await ytpl(channel.url + '', {limit: 0});

        if(results && results.items && results.items.length) {
            let playlist = results.items;

            for(let v of playlist) {
                let is_consumed = consumed.includes(v.id);
                let is_downloading = videos_downloading.includes(v.id);
                let is_failure = failures.includes(v.id);
                let is_queued = !!videos_to_download.filter(videos => videos.id === v.id).length;

                //if queued or consumed already, ignore it.
                if(is_consumed || is_queued || is_downloading || is_failure) {
                    continue;
                }

                count++;

                let clean_name = v.title.trim().replace(/'/g, '');

                let data = {
                    friendly: channel.friendly,
                    id: v.id,
                    name: clean_name,
                    url : v.url_simple || v.url
                };

                if(channel.dlpath) {
                    data.dlpath = channel.dlpath
                }
                videos_to_download.push(data)
            }
        }
    } catch(e){
        console.log('error.get_playlist', e);
    }

    if(count) {
        mlog.debug('ADDED TO QUEUE', count);
        firstload = false;
    }
}

/**
 * Downloads the video.
 */
function get_video(video) {
    let url = video.url;
    let filename = path.resolve(__dirname, `./tmp/id_${video.id}_video.mp4`);

    mlog.debug('VIDEO:', video.name);

    return function(callback){
        try {
        ytdl(url + '', { filter:"videoonly",quality:"highestvideo" })
            .pipe(fs.createWriteStream(filename))
            .on('finish', function(){
                download_state[video.id].video = filename;
                callback(null, true);
            });
        } catch (e) {
            callback(e)
        }
    };
}

/**
 * Downloads the audio.
 */
function get_audio(video) {
    let url = video.url;
    let filename = path.resolve(__dirname, `./tmp/id_${video.id}_sound.m4a`);

    mlog.debug('AUDIO:', video.name);

    return function(callback){
        try {
        ytdl(url + '', {filter:"audioonly",quality:"highestaudio"})
            .pipe(fs.createWriteStream(filename))
            .on('finish', function(){
                download_state[video.id].audio = filename;
                callback(null, true);
            });
        } catch (e) {
            callback(e)
        }
    };
}

/**
 * Uses ffmpeg to merge audio and video streams together.
 */
async function merge_streams(video) {
    download_state[video.id].step = 'merging';
    let download_path = config.filepath;
    let download_dir = '';

    if(download_path.startsWith('.')){
        download_dir = path.join(__dirname, download_path, video.friendly);
    } 
    else if(video.dlpath) {
        download_dir = path.join(dlpath, video.friendly);
    }
    else {
        download_dir = path.join(download_path, video.friendly);
    }
    
    try{
        await fs.promises.access(download_dir);
    } catch(e) {
        await fs.promises.mkdir(download_dir);
    }   

    let audio_file = path.resolve(__dirname, `./tmp/id_${video.id}_sound.m4a`);
    let video_file = path.resolve(__dirname, `./tmp/id_${video.id}_video.mp4`);

    let filename = video.name.replace(/\.|\?|\\|\||\"/ig, '').replace(/\||\//ig, ' - ') + '.mp4';
    let fullpath = path.join(download_dir, filename);

    let progressHandler = function(progress) {
        let v = video;
        if(progress.percent) {
            download_state[v.id].merge_percent = progress.percent;
        } else {
            download_state[v.id].merge_percent = progress.timemark || progress.frames;
        }
    }

    let p = new Promise((resolve, reject) => {
        mlog.debug('MERGING:', filename);
        ffmpeg()
        .input(video_file)
        .input(audio_file)
        .outputOptions(['-preset veryfast'])
        .on('progress', progressHandler)
        .on('error',function(e){
            mlog.error(e);
            reject(false);
        })
        .on('end', async function() {
            resolve(true);
        })
        .save(fullpath);

    });
    return p;
}

/**
 * Execution flow of everything needed to download a video.
 */
async function start_process(video) {
    let startTime = Date.now();
    downloading_count++;

    download_state[video.id] = {
        obj: video,
        step: 'getting info',
        video: 'missing',
        audio: 'missing',
        merge_percent: 0
    };

    //check video status, if value just continue on
    try {
        if(video.url) {
            let info = await ytdl.getInfo(video.url + '');
        }
        else {
            throw "nope exception"
        }
    } catch(e) {
        await db.get('failures').push(video.id).write();

        downloading_count--;
        delete download_state[video.id];

        mlog.debug('UNAVAILABLE:', video.name);
        mlog.error(e);
        return;
    }

    //get audio and video in parallel
    download_state[video.id].step = 'downloading';
    async.parallel({
        video: get_video(video),
        audio: get_audio(video)
    },
    async function(err, results){
        let failed = false;

        if(err) {
            failed = true;
            mlog.error(err);

            mlog.debug('REDOWNLOAD (failed download):', video.name);
            await db.get('failures').push(video.id).write();
            delete download_state[video.id];

        }
        else if(results.video && results.audio){
            try {
                //merge audio/video
                let results = await merge_streams(video);
            } catch(e) {
                mlog.error(e);
                failed = true;
                mlog.debug('REDOWNLOAD (failed merge):', video.name);
                await db.get('failures').push(video.id).write();
                delete download_state[video.id];

            }
        }

        //clean up files
        let audio_file = path.resolve(__dirname, `./tmp/id_${video.id}_sound.m4a`);
        let video_file = path.resolve(__dirname, `./tmp/id_${video.id}_video.mp4`);

        try {
            await fs.promises.unlink(video_file);
        } catch (e) {
            //do nothing, just don't crash
        }

        try {
            await fs.promises.unlink(audio_file);
        } catch (e) {
            //do nothing, just don't crash
        }

        //we don't want to log a success to the db
        if(!failed) {
            await db.get('consumed').push(video.id).write();
        }

        videos_downloading = videos_downloading.filter(item => item !== video.id);
        downloading_count--;
        delete download_state[video.id];
        let endTime = Date.now();
        let lengthms = ((endTime - startTime) / 1000 / 60).toFixed(2);
        mlog.debug(`FINISHED (${lengthms}m):`, video.name);
        await notify.send('Youtuber', `${video.name}`);
    });
}

/**
 * Loops application to check if there are new videos. If there are
 * spawn new works to download multiple videos at once.
 */
async function download_loop() {
    if(videos_to_download.length > 0 && !pause) {
        if(downloading_count < max_downloaders) {
            let video = videos_to_download.shift();
            videos_downloading.push(video.id);
            
            setTimeout(async function(){
                start_process(video);
            }, 500);
        }
    } 

    setTimeout(download_loop, 1000 * 5); //5s
};

/**
 * Gets the playlist every hour.
 */
async function playlist_loop() {
    let consumed = await db.get('consumed').cloneDeep().value();
    let channels = await db.get("channels").cloneDeep().value();
    let failures = await db.get("failures").cloneDeep().value();

    for(let channel of channels) {
        await get_playlist(channel, consumed, failures);
    }

    mlog.debug('TOTAL QUEUE:', videos_to_download.length);

    setTimeout(playlist_loop, 1000 * 60 * 60 * 6); //6hr
}

/**
 * Output what's going on, what's left
 */
function stats() {
    mlog.debug('------------------STATS------------------');
    mlog.debug('TOTAL QUEUE:', videos_to_download.length);
    mlog.debug('IN PROGRESS:', downloading_count);
    
    for(let key of Object.keys(download_state)) {
        let v = download_state[key];
        console.log();
        mlog.debug('VIDEO:', v.obj.name);
        mlog.debug('URL', v.obj.url);
        mlog.debug('STEP', v.step);
        mlog.debug('VIDEO FILE', v.video);
        mlog.debug('AUDIO FILE', v.audio);
        mlog.debug('ENCODING', v.merge_percent);
    }

    mlog.debug('-----------------------------------------');

}

/**
 * Clears the tmp folder before downloading
 */
async function clean_up(){
    let files = await fs.promises.readdir('./tmp');
    
    for(let file of files) {
        if(file.includes('.m4a') || file.includes('.mp4')) {
            await fs.promises.unlink(`./tmp/${file}`);
        }
    }
}

/**
 * Starts the application.
 */
async function start_up(){
    const adapter = new FileAsync('./db.json');
    db = await low(adapter);

    db.defaults(config.dataDefaults).write();

    await clean_up();

    download_loop();
    playlist_loop();

    setInterval(stats, 1000 * 60 * 5); //5min
}

fastify.listen(3000, err => {
    if (err) throw err

    console.log(`server listening on ${fastify.server.address().port}`)
    start_up();

});