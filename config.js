let config = {
    dataDefaults: {
        consumed: [],
        channels: [],
        failures: [],
        filepath: "./videos"
    },
    filepath: "./videos",
    max_download: 3
};

module.exports = config;
