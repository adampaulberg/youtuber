let chalk = require('chalk');
let log = console.log;
let bluebird = require('bluebird');
let figlet = bluebird.promisify(require('figlet'));
let _ = require('underscore');

function info(text) {
    text = _toString(text);
    log(chalk.blue(text))
}

function error(a, b) {
    let newa = _toString(a);  
    let newb = _toString(b); 

    if(b) {
        log(chalk.red(newa), chalk.cyan(newb));    
    } else {
        log(chalk.red(newa));
    }
}

function warning(text) {
    text = _toString(text);  
    log(chalk.yellow(text))
}

function good(text) {
    text = _toString(text);
    log(chalk.green(text));
}

function debug(a, b) {
    let newa = _toString(a);  
    let newb = _toString(b); 

    if(b) {
        log(chalk.cyan(newa), chalk.magenta(newb));    
    } else {
        log(chalk.magenta(newa));
    }
}

function custom(a, acolor,  b, bcolor) {
    let newa = _toString(a);  
    let newb = _toString(b); 

    if(b && bcolor) {
        log(chalk.keyword(acolor)(newa), chalk.keyword(bcolor)(newb));    
    } else if(a && acolor){
        log(chalk.keyword(acolor)(newa));
    } else {
        warning('improper use of custom log')
    }
}

function _toString(obj)  {
    switch (typeof obj) {
        case 'object':
            if('stack' in obj) {
                return obj.stack;
            }
            return JSON.stringify(obj);
        case 'function':
          return obj.toString();
        default:
          return obj + '';
      }
}

async function title(title, font) {
    let text = await figlet(title, {font: font || 'Elite'});
    log()
    log(chalk.cyan(text));
}

async function asyncFiglet(title, font) {
    let text = await figlet(title, {font: font || 'Standard'});
    log(text);
}

module.exports = {
    info: info,
    error: error,
    warning: warning,
    debug: debug,
    good: good,
    custom: custom,
    title: title,
    asyncFiglet: asyncFiglet
};
