const chump = require('chump');

const client = new chump.Client('ayx1ixhg37ooqf1gwubmntah41iqpi');
const user = new chump.User('u6d8mnenwrmt1sw3zdi9kozdyyds7j');

/**
 * Sends a notification via Pushover to device.
 */
async function send(title, message) {
    try {
        let chump_message = new chump.Message({
            title:      title,
            message:    message,
            enableHtml: false,
            user:       user,
            priority:   new chump.Priority('low'),
            sound:      new chump.Sound('magic')
        });

        let results = await client.sendMessage(chump_message);
    } catch (e) {
        mlog.error('util.notifications.' + arguments.callee.name, ex);

        return false;
    }
}

/**
 * Send notification to users
 */
async function send_user(api, user, title, message) {
    try {
        const n_client = new chump.Client(api);
        const n_user = new chump.User(user);

        let chump_message = new chump.Message({
            title:      title,
            message:    message,
            enableHtml: false,
            user:       n_user,
            priority:   new chump.Priority('low'),
            sound:      new chump.Sound('magic')
        });

        let results = await n_client.sendMessage(chump_message);
    } catch (e) {
        mlog.error('util.notifications.' + arguments.callee.name, ex);

        return false;
    }
}

module.exports = {
    send: send,
    send_user: send_user
};